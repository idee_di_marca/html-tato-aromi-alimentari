/*

Description: Ajax Navigation Custom for Tato Aromi Alimentari
Author: Idee di Marca
Author URI: https://www.ideedimarca.it/
Version: 1.0

*/

"use strict";

(function ($) {

    var firstLoad;
    
    var isAnimating = false,
        newLocation = '';
        firstLoad = false;
    
    // trigger smooth transition from the actual page to the new one 
    $('body').on('click', '[data-type="page-transition"]', function(event){
        event.preventDefault();

        // check if menu is opened
        if( $('body').hasClass('menu-opened') ) {
            $('body').removeClass('menu-opened');
        }

        // scroll body to top before animating
        $("html, body").animate({ scrollTop: 0 }, 500);

        // detect which page has been selected
        var newPage = $(this).attr('href');
      
        // if the page is not already being animated - trigger animation
        if( !isAnimating ) changePage(newPage, true);

        firstLoad = true;

    });

    $('body').on('click' , '.product-heading' , function(){
        $(this).data('clicked' , true);

        $('body').addClass( $(this).attr('data-product') );

        if ($(this).data('clicked')) {
            $(this).parent().addClass('current');
        }
        if( ! $(this).data('clicked') ) {
            $(this).parent().addClass('not-current');
        }

    });
  
    // detect the 'popstate' event - e.g. user clicking the back button
    $(window).on('popstate', function() {
        if( firstLoad ) {
          
            var newPageArray = location.pathname.split('/'),
            //this is the url of the page to be loaded 
            newPage = newPageArray[newPageArray.length - 1];
  
            if( !isAnimating  &&  newLocation != newPage ) changePage(newPage, false);

            changePage(newPage, false)
        }
        firstLoad = true;
    });
  
    function changePage(url, bool) {
        isAnimating = true;
      
        // trigger page animation
        $('body').addClass('page-is-changing');

        // load new content
        loadNewContent(url, bool);
        newLocation = url;

        //if browser doesn't support CSS transitions
        if( !transitionsSupported() ) {
            loadNewContent(url, bool);
            newLocation = url;
        }
    }
  
    function loadNewContent(url, bool) {
        
        url = ('' == url) ? 'index.html' : url;
        
        var newSection = url.replace('.html', '');
        
        var section = $('<div class="content-product content-product- '+newSection+'"></div>');
            
        section.load(url+' .cd-main-content > *', function(event){
        
        //if browser doesn't support CSS transitions - dont wait for the end of transitions
        var delay = ( transitionsSupported() ) ? 2500 : 0;
        
        setTimeout(function(){

            // load new content and replace <main> content with the new one
            $('main').html(section);

            // wait for the end of the transition on the loading bar before revealing the new content
            // ( section.hasClass('cd-about') ) ? $('body').addClass('cd-about') : $('body').removeClass('cd-about');

            // $('body').removeClass('page-is-changing');
            $('body').removeClass();
            $('body').addClass(newSection);


            isAnimating = false;
  
            if( !transitionsSupported() ) isAnimating = false;

        }, delay);
        
        if( url != window.location && bool ){

            //add the new page to the window.history
            //if the new page was triggered by a 'popstate' event, don't add it
            window.history.pushState({path: url},'',url);
            }
        });
    }
  
    function transitionsSupported() {
      return $('html').hasClass('csstransitions');
    }

})(jQuery);