/*

Description: Custom JS for Tato Aromi Alimentari
Author: Idee di Marca
Author URI: https://www.ideedimarca.it/
Version: 1.0

*/

"use strict";

(function ($) {

    // hero nav 
    function handleHeroNav() {
        $('.menu-toggler').on('click' , function(evt){
            evt.preventDefault();
            $('body').toggleClass('menu-opened')
        });
    }

    // carousels
    function handleCarousels() {

        $('.carousel').each(function(i){
            $(this).slick({
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                arrows: false,
                dots: false,
                autoplay: true,
                autoplaySpeed: 5000,
                speed: 800,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
        });

        // Custom carousel nav
	    $('.carousel-nav').find('.prev-slide').on('click' , function() {
            $('.carousel').slick('slickPrev');
        })
        $('.carousel-nav').find('.next-slide').on('click' , function() {
            $('.carousel').slick('slickNext');
        })

    }

    // animations
    function handleAnimations() {

        // hero nav - other pages transition
        $('nav.hero-nav').find('a.menu-item').on('click', function(evt){
            evt.preventDefault();
    
            var href = $(this).attr('href');
            var dataType = $(this).attr('data-type');
            var currentLocation = window.location.pathname;
            var destinationLocation = '/nuovo/' + href;

            if ( dataType === 'page') {

                // close hero nav
                $('body').removeClass('menu-opened');

                // scroll view to top
                $("html, body").animate({ scrollTop: 0 }, 500);

                if( currentLocation !== destinationLocation ) {

                    // init fade out transition
                    setTimeout(function(){
                        $('body').addClass('loading');
                    }, 500);
                    
                    // navigate to destination after delay
                    setTimeout(function(){
                        window.location = href
                    }, 1500);
                    
                }

            }
            else {

                // close hero nav
                $('body').removeClass('menu-opened');

                // scroll view to top
                $("html, body").animate({ scrollTop: 0 }, 500);

                if( currentLocation !== destinationLocation ) {

                    if( $('body').hasClass('innerpage') ) {

                        // init fade out transition
                        setTimeout(function(){
                            $('body').addClass('loading');
                        }, 500);

                    }

                }

            }

            if( currentLocation === destinationLocation ) {

                // close hero nav
                // $('body').removeClass('menu-opened');

            }
    
        });

        // main logo - other pages transition
        $('a[has-transition="page"]').on('click', function(evt){
            evt.preventDefault();
    
            var href = $(this).attr('href');
            var currentLocation = window.location.pathname;
            var destinationLocation = '/nuovo/' + href;

            // scroll view to top
            // $("html, body").animate({ scrollTop: 0 }, 500);

            if( currentLocation !== destinationLocation ) {

                if( $('body[class*="single-product"]') ) {

                    // init fade out transition
                    setTimeout(function(){
                        $('body').addClass('loading');
                    }, 500);

                    // navigate to destination after delay
                    setTimeout(function(){
                        window.location = href
                    }, 1500);

                }

            }
    
        });

        // scroll top on tabs click
        $('#pills-types-tab').find('button').on('shown.bs.tab', function () {
            $('html, body').animate({
                scrollTop: $("#pills-types-tab").offset().top
            });
        });
        
        // longer loader for front page
        if( $('body').hasClass('front-page') ) {
        
            // preloading content
            setTimeout(function(){
                $('body').removeClass('loading')
            }, 2250);

        }
        else {
            
            // preloading content
            setTimeout(function(){
                $('body').removeClass('loading')
            }, 100);

        }

        // wrap title with <span>
        $(".preloading-header").find('h1').each(function (index) {
            var characters = $(this).text().split("");
            var $this = $(this);
            $this.empty();
            $.each(characters, function (i, el) {
                $this.append('<div style="animation-delay:' + i * 75 + 'ms""><span>' + el + '<span></div');
            });
         });

        // set body always to top
        $("html, body").animate({ scrollTop: 0 }, 500);

    }
    
    // hero nav
    handleHeroNav();

    // carousels
    handleCarousels();

    // animations
    handleAnimations();

})(jQuery);